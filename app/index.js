'use strict';

var SkypeApi = require('./skypeapi/skype.api');
var config = require('./config');
var async = require('async');

var skypeApi = new SkypeApi(config.login, config.password);

// Логинимся
skypeApi.doLogin(function() {
  // Входим бесконечный цикл
  async.forever(function(next) {
    // Получаем сообщения
    skypeApi.getMessages(function(err, response) {
      // Если ответ есть и сообщения тоже
      if (response && response.eventMessages) {
        // Проходимся по сообщениям
        async.each(response.eventMessages, function(message, _next) {
          // console.log(message);
          // Ищем только те сообщения, которые имеют нижеуказанный тип
          if (message.resourceType === 'NewMessage' && message.resource.content) {
            // Если сообщение отослал я и сообщение содержит в первых 4-х символах `%bot`
            var parse = message.resource.content.match(/%bot\s([\w\s]+)\:*(.*)/i);
            if (skypeApi.isMe(message.resource.from) && parse && Object.keys(config.commands).indexOf(parse[1]) >= 0) {
              require('./modules/' + config.commands[parse[1]])(skypeApi, parse[2].trim(), message);
            }
          }
          _next(null);
        }, next);
      } else {
        next(err);
      }
    });
  });
});

/*
messageType: {
  Text,
  RichText,
  RichText/Contacts,
  RichText/Files,
  RichText/Sms,
  RichText/Location,
  RichText/UriObject,
  RichText/Media_FlikMsg,
  Event/SkypeVideoMessage,
  ThreadActivity/AddMember,
  ThreadActivity/DeleteMember,
  ThreadActivity/RoleUpdate,
  ThreadActivity/TopicUpdate,
  ThreadActivity/PictureUpdate,
  ThreadActivity/HistoryDisclosedUpdate,
  ThreadActivity/JoiningEnabledUpdate,
  ThreadActivity/LegacyMemberAdded,
  ThreadActivity/LegacyMemberUpgraded,
  Event/Call,
  Control/Typing,
  Control/ClearTyping,
  Control/LiveState
}
*/
