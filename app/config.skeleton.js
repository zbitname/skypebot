'use strict';

module.exports = {
  login: 'login',
  password: 'password',
  commands: {
    ping: 'ping.pong',
    'to html': 'text.to.html',
    jira: 'jira'
  },
  modules: {
    jira: {
      hostname: 'jira.domain.com',
      ssl: true,
      login: 'login',
      password: 'password'
    }
  }
};
