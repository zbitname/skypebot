'use strict';

var SkypeApi = require('./skype.api');
var config = require('./../config');

var skypeApi = new SkypeApi(config.login, config.password);


skypeApi.doLogin(function() {
  skypeApi.setSubscriptions(function() {
    skypeApi.getMessages(function(err, messages) {
      console.log('getMessages', err, messages);
    });
  });
});
