'use strict';

function SkypeError(msg) {
  this.message = msg;
}

SkypeError.prototype = Error.prototype;

module.exports = SkypeError;
