'use strict';

module.exports = function(skypeApi, arg, message) {
  skypeApi.writeResponse(message, '<i>-></i> ' + arg, function(err) {
    if (err) { console.log(err); }
  });
};